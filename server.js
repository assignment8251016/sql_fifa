const {Client} = require("pg");
require('dotenv').config()
const matchesPlayedPerCity = require("./sql/matchesPlayedPerCity.cjs");
const numberOfMatchesWonPerTeam = require("./sql/numberOfMatchesWonPerTeam.cjs");
const refereeOfficiatedMultipleTimes = require("./sql/refereeOfficiatedMultiple.cjs");
const numberOfGoalsScoredEachMinute = require("./sql/numberOfGoalsScoredEachMinute.cjs")
const top10playersPlayedMostMatches = require("./sql/top10playersPlayedMostMatches.cjs")
const top8TeamsScroedMostGoalsInGermany = require("./sql/top8TeamsScroedMostGoalsInGermany.cjs");
const writeFile = require("./helpers/writeFIle.js");
const client = new Client({
      host:process.env.HOST,
      port:process.env.PORT,
      database:process.env.DATABASE
});

client.connect((err)=>{
      if(err){
            console.log("Could not build connection, err", err);
      }
})

matchesPlayedPerCity(client)
      .then(data=>{
            writeFile("matches_played_per_city.json",JSON.stringify(data.rows),"./results/")
      });

numberOfMatchesWonPerTeam(client)
      .then((data)=>{
            writeFile("number_of_matches_won_per_team.json",JSON.stringify(data.rows),"./results/")
            
      })


refereeOfficiatedMultipleTimes(client)
      .then(data=>{
            writeFile("referees_who_have_officiated_in_multiple_world_cups.json", JSON.stringify(data.rows), "./results/")
            
      })
      
numberOfGoalsScoredEachMinute(client)
      .then(data=>{
            const obj = {}
            data.rows.forEach(time=>obj[time["time"]]=time["Goal Scored"])
            writeFile("number_of_goals_scored_each_minute.json", JSON.stringify(obj), "./results/")
            
      })
top10playersPlayedMostMatches(client)
      .then(data=>{
            writeFile("top10_players_who_played_most_matches_without_red_yellow_cards.json", JSON.stringify(data.rows), "./results/")
      })
      
top8TeamsScroedMostGoalsInGermany(client)
      .then(data=>{
            
            const arr= []
            data.rows.forEach((ele)=>{
                  const obj={}
                  obj[ele["home_team_name"]]=ele["Goals Scored"];
                  arr.push(obj)
            })
            writeFile("top8_teams_scored_most_number_of_goals_in_germany.json", JSON.stringify(arr), "./results/")
            .then(()=>client.end())
      })