module.exports = function printTableColumns(client, tableName) {
    client.query(`
  SELECT column_name
  FROM information_schema.columns
  WHERE table_name = $1`, [tableName], (err, res) => {
        if (err) throw err;
        const columnNames = res.rows.map(row => row.column_name);
        console.log(columnNames);

    });

}