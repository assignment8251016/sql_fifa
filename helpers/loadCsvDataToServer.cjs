var { from } = require('pg-copy-streams')
const fs = require("fs")

module.exports = async function loadCsvDataToServer(file_path, client, table_name, columnsString) {
    const fileStream = fs.createReadStream(file_path);
    const copyQuery = `
      COPY ${table_name} (${columnsString})
      FROM STDIN WITH (FORMAT csv, HEADER true)
    `;
    const stream = client.query(from(copyQuery));
    fileStream.pipe(stream)
        .on('finish', () => {
            console.log('CSV data loaded into PostgreSQL');
            client.end();
        })
        .on('error', err => {
            console.error('Error loading CSV data into PostgreSQL', err);
            client.end();
        });
}
