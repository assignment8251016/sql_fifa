module.exports = function numberOfMatchesWonPerTeam(client){
    return client.query(`
        SELECT  home_team_name, home.goals+away.goals AS goals FROM ((
            SELECT home_team_name, COUNT(home_team_name) AS goals
            FROM worldcupmatches 
            WHERE home_team_goals>away_team_goals GROUP BY home_team_name
            ) AS home 
            JOIN 
            (
                SELECT away_team_name, COUNT(away_team_name) AS goals
                FROM worldcupmatches
                WHERE away_team_goals>home_team_goals GROUP BY away_team_name
            ) AS away  
            ON home.home_team_name = away.away_team_name )      
    `)
}

