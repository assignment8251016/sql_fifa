module.exports = function top8TeamsScroedMostGoalsInGermany(client){
    return client.query(`
      SELECT
        home_team_name,
        home.home_goals + away.away_goals AS "Goals Scored"
      FROM (SELECT
        home_team_name,
        SUM(home_team_goals) AS home_goals
      FROM worldcupmatches
      WHERE year IN (SELECT
        year
      FROM worldCups
      WHERE country = 'Germany')
      GROUP BY home_team_name) home
      JOIN (SELECT
        away_team_name,
        SUM(away_team_goals) AS away_goals
      FROM worldcupmatches
      WHERE year IN (SELECT
        year
      FROM worldCups
      WHERE country = 'Germany')
      GROUP BY away_team_name) away
        ON away_team_name = home_team_name
      ORDER BY "Goals Scored" DESC LIMIT 8;
    `);
}