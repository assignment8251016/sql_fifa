module.exports = function refereeOfficiatedMultiple(client){
    return client.query(`
        SELECT
          referee,
          COUNT(referee) AS "World Cups"
        FROM (SELECT DISTINCT
          referee,
          year
        FROM worldcupmatches) AS t1
        GROUP BY referee
        ORDER BY COUNT(referee) DESC, referee ASC;
    `)
}