module.exports = function matchesWonPerYear(client){
    return client.query(`
        SELECT
           city,
           COUNT(city) 
        FROM
           worldcupmatches 
        WHERE
           city IS NOT NULL 
        GROUP BY
           city
    `);
}