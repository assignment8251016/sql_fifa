module.exports = function numberOfGoalsScoredEachMinute(client){
    return client.query(`
        SELECT 
            time,count(time) as \"Goal Scored\" 
        FROM 
            (SELECT substring(event, 2, length(event)-2)::integer AS time 
        FROM 
            (SELECT unnest(regexp_split_to_array(event, ' ')) as event 
        FROM 
            worldcupplayers 
        WHERE 
            event IS NOT NULL) as T WHERE T.event LIKE 'G%') as T 
        GROUP BY 
            time 
        ORDER BY 
            time`
    )}