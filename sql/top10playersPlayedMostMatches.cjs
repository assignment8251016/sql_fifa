module.exports = function top10playersPlayedMostMatches(client){
    return client.query(`
    SELECT 
        t1.player_name, 
        t1.matches 
    FROM 
    (
      SELECT 
        player_name, 
        Count(player_name) AS matches 
      FROM 
        worldcupplayers 
      GROUP BY 
        player_name
    )  t1 
    JOIN (
      SELECT 
        player_name, 
        Count(player_name) AS matches 
      FROM 
        worldcupplayers 
      WHERE 
        event NOT LIKE '%R%' 
        AND event NOT LIKE '%Y%' 
      GROUP BY 
        player_name
        )  t2 ON t1.player_name = t2.player_name 
    WHERE 
        t1.matches = t2.matches 
    ORDER BY 
        t1.matches DESC, 
        t1.player_name ASC 
    LIMIT 
        10
    `)
}

